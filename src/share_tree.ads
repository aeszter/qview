with Slurm.Share_Tree;

-- This package provides subprograms to output information on the fair
-- share status
package Share_Tree is
   procedure Put_All (Sort_Field : String; Sort_Direction : String);
   -- output a list of all users, optionally sorting by any column
   -- @param Sort_Field the name of the column to sort by; if it is
   -- the empty string, the list is unsorted
   -- @param Sort_Direction is either "inc", "dec", or unset; if
   -- "dec", the list will be sorted in descending order
   procedure Sort_By (Field : String; Direction : String);
   -- sort the list of all users
   -- @param Field the name of the column to sort by; if it is
   -- the empty string, the list is unsorted
   -- @param Direction is either "inc", "dec", or unset; if
   -- "dec", the list will be sorted in descending order

private
   procedure Put (Item : Slurm.Share_Tree.Lists.Cursor);

end Share_Tree;
