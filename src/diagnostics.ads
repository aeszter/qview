-------------------------------------------------------
--  This package includes code from the Florist project
-------------------------------------------------------

package Diagnostics is
  -- This package collects some data about program execution and
  -- displays it as CGI output

   procedure Put_Time;
   -- Output how much time the program and any child processes have
   -- taken

   procedure Put_Date;
   -- Output the current date and time

   procedure Put_Memory;
   -- Output how much memory the program has used

end Diagnostics;
