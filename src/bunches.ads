with Slurm.Bunches;

-- @summary Bunches are sets of jobs sharing requirements
package Bunches is

   procedure Put (B : Slurm.Bunches.Bunch);
   -- output a single bunch as a row in a table
   -- @param B the bunch to output
   procedure Put_List (Source : Slurm.Bunches.List);
   -- output a list of bunches as a table
   -- @param Source the list of bunches to output

   procedure Put_All;
   -- output all bunches as a table

end Bunches;
