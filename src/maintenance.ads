with Nodes;

package Maintenance is
   procedure Put_All;
private
   procedure Put_Error_Messages;
   procedure Put_Header;
   procedure Put_High_Load;
   procedure Put_Low_Load;
   procedure Put_Drained;
   procedure Put_Unreachable;
   procedure Put_Offline;

   function High_Load (N : Nodes.Node) return Boolean;
   function Low_Load (N : Nodes.Node) return Boolean;
--     function High_Swap (H : Hosts.Host) return Boolean;
--     function No_Queue (H : Hosts.Host) return Boolean;
   function Reachable_Disabled (N : Nodes.Node) return Boolean;
   function Unreachable_Enabled (N : Nodes.Node) return Boolean;
   function Offline (N : Nodes.Node) return Boolean;
end Maintenance;
