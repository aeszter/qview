with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

-- @summary This package is the entry package of Qview. It contains
-- the most high-level routines.
package Viewer is
   procedure View;
   -- the main routine; outputs an HTML page.
   procedure Set_Params (Params : String);
   -- Set a list of parameters. This list will be included in every
   -- link, so user seletions can be made persistent across hyperlinks
   -- by adding the necessary CGI parameters here.
   -- @param Params the parameters to record; should be of the form
   -- "param1=value1&param2=value2..."
   procedure Append_Params (Params : String);
   --  Purpose: Add a parameter to the list kept to create links that preserve
   --          user selections
   --  @param Params the parameter to add; should be of the form "param=value"
   function Params return String;
   -- get recorded parameters
   -- @return the parameters recorded so far
   procedure Put_Error (Message : String);
   -- output an HTML page indicating an error; this should be called
   -- for fatal errors (i.e. when normal output is impossible) only
   -- @param Message the error message to show to the user
   procedure Put_Result (Message : String);
   -- Output an HTML page indicating a special state, but not an error;
   -- this should be called when there is no normal output only.
   -- Normally, this is used to indicate that an action (such as
   -- draining a node) has been triggered by the user, and what the
   -- result of that action is.
   -- @param Message the message to show to the user
private
   My_Params : Unbounded_String;
   Sort_Direction : String := "inc";
   Sort_String    : Unbounded_String := Null_Unbounded_String;
   function Sort_Field return String;
end Viewer;
