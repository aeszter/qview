with Ada.Text_IO;
with Ada.Exceptions; use Ada.Exceptions;
with Slurm.Partitions; use Slurm.Partitions;
with Utils;
with HTML;

package body Partitions is

   procedure Put (Position : Slurm.Partitions.Cursor);
   procedure Put_Error (Message : String);

   function Explain_Mode (M : Slurm.Partitions.Preempt_Mode) return String is
   begin
      case M is
         when preempt_off => return "no premption";
         when preempt_suspend => return "suspend preempted job";
         when preempt_requeue => return "preempted job will be restarted from the beginning";
         when preempt_cancel => return "preempted job will be cancelled";
         when preempt_qos => return "preemption depends on QOS";
         when preempt_gang => return "gang scheduling";
      end case;
   end Explain_Mode;

   procedure Put (Position : Slurm.Partitions.Cursor) is
      P : Partition;
   begin
      if Has_Element (Position) then
         P := Element (Position);
      else
         raise Constraint_Error with "no such partition";
      end if;
      if P.Has_Errors then
         Ada.Text_IO.Put ("<tr class=""program_error"">");
         P.Iterate_Errors (Put_Error'Access);
      else
         Ada.Text_IO.Put ("<tr>");
      end if;
      HTML.Put_Cell (Data => Get_Name (P),
                     Link_Param => "partitions");
      HTML.Put_Cell (Data =>  Get_Node_Number (P)'Img, Class => "right");
      HTML.Put_Cell (Data => Get_CPUs_Per_GPU (P)'Img, Class => "right");
      Ada.Text_IO.Put ("</tr>");
   exception
      when --  E :
         others =>
         --           HTML.Error ("Error while putting host "
--           & SGE.Host_Properties.Value (Get_Name (H)) & ": "
--                       & Exception_Message (E));
         Ada.Text_IO.Put ("</tr>");
   end Put;

   procedure Put_All is
   begin
      Put_List (Slurm.Partitions.Load_Partitions);
   end Put_All;

   procedure Put_Details (Name : String) is
      procedure Put_Meta;
      procedure Put_Access;
      procedure Put_Preempt_Mode;
      procedure Put_Resources;

      The_List : constant Slurm.Partitions.List :=
        Slurm.Partitions.Load_Partitions;
      P        : constant Partition := Slurm.Partitions.Get_Partition (The_List, Name);

      procedure Put_Access is
      begin
         HTML.Put_Heading ("Access", 3);
         HTML.Put_Paragraph ("Allocation nodes", Get_Allocation_Nodes (P));
         HTML.Put_Paragraph ("Accounts", Get_Accounts (P));
         HTML.Put_Paragraph ("Deny", Get_Deny_Accounts (P));
         HTML.Put_Paragraph ("Groups", Get_Groups (P));
         HTML.Put_Paragraph ("QOS", Get_QOS (P));
         HTML.Put_Paragraph ("Deny QOS", Get_Deny_QOS (P));
         HTML.Put_Paragraph ("Alternate", Get_Alternate (P));
         HTML.Put_Clearer;
      end Put_Access;

      procedure Put_Meta is
      begin
         HTML.Put_Heading ("Flags", 3);
         HTML.Put_List_Head;
         if P.Is_Default then
            HTML.Put_List_Entry ("default");
         end if;
         if P.Is_Hidden then
            HTML.Put_List_Entry ("hidden");
         end if;
         if P.Is_No_Root then
            HTML.Put_List_Entry ("no root");
         end if;
         if P.Is_Root_Only then
            HTML.Put_List_Entry ("root only");
         end if;
         if P.Requires_Reservation then
            HTML.Put_List_Entry ("reservation required");
         end if;
         if P.Is_Least_Loaded then
            HTML.Put_List_Entry ("least loaded");
         end if;
         if P.Is_Exclusive_User then
            HTML.Put_List_Entry ("exclusive user");
         end if;
         if P.Is_Idle_Power_Down then
            HTML.Put_List_Entry ("power down when idle");
         end if;
         HTML.Put_List_Tail;
         HTML.Put_Heading ("Consumables", 3);
         if P.Has_CR_CPU then
            Ada.Text_IO.Put_Line ("CPU");
         end if;
         if P.Has_CR_Socket then
            Ada.Text_IO.Put_Line ("Socket");
         end if;
         if P.Has_CR_Core then
            Ada.Text_IO.Put_Line ("Core");
         end if;
         if P.Has_CR_Board then
            Ada.Text_IO.Put_Line ("Board");
         end if;
         if P.Has_CR_Memory then
            Ada.Text_IO.Put_Line ("Memory");
         end if;

         Put_Preempt_Mode;
         HTML.Put_Paragraph ("Priority factor", Get_Priority_Job_Factor (P)'Img);
         HTML.Put_Paragraph ("Priority tier", Get_Priority_Tier (P)'Img);
         HTML.Put_Paragraph ("QOS", Get_QOS_Name (P));
         HTML.Put_Paragraph ("State", (HTML.Img_Tag (Get_State_Up (P)'Img)));
         HTML.Put_Paragraph ("CPUs", Get_Total_CPUs (P)'Img);
         HTML.Put_Paragraph ("Nodes", Get_Total_Nodes (P)'Img);
         HTML.Put_Paragraph ("TRES", Utils.Insert_Break (Get_TRES (P), ','));
         HTML.Put_Clearer;
      end Put_Meta;

      procedure Put_Preempt_Mode is
         procedure Put (What : String) renames Ada.Text_IO.put;
      begin
         Put ("<p>Preempt Mode: ");
         Put ("<img src=""/icons/" & Get_Preempt_Mode (P) & ".png"" ");
         Put ("alt=""" & Get_Preempt_Mode (P) & """ title=""" & Get_Preempt_Mode (P) & ": ");
         Put (Explain_Mode (Get_Preempt_Mode (P)));
         Put (""" /></p>");
      end Put_Preempt_Mode;

      procedure Put_Resources is
         use Utils;
      begin
         HTML.Put_Heading ("Resources", 3);
         HTML.Put_Paragraph ("Billing Weights", Get_Billing_Weights (P));
         HTML.Put_Paragraph ("CPUs per GPU", Get_CPUs_Per_GPU (P)'Img);
         HTML.Put_Paragraph ("Memory per CPU", Get_Default_Memory_Per_CPU (P)'Img);
         HTML.Put_Paragraph ("Default Time", Get_Default_Time (P));
         HTML.Put_Paragraph ("Grace time", Get_Grace_Time (P));
         HTML.Put_Paragraph ("Max CPUs per node", To_String (Get_Max_CPUs_Per_Node (P)));
         HTML.Put_Paragraph ("Max memory per node", Get_Max_Memory_Per_CPU (P)'Img);
         HTML.Put_Paragraph ("Max nodes", To_String (Get_Max_Nodes (P)));
         HTML.Put_Paragraph ("Max share", Get_Max_Share (P)'Img);
         HTML.Put_Paragraph ("Max time", Get_Max_Time (P));
         HTML.Put_Paragraph ("Min nodes", To_String (Get_Min_Nodes (P)));
         HTML.Put_Clearer;
      end Put_Resources;

   begin
      HTML.Begin_Div (Class => "partition_info");
      HTML.Begin_Div (Class => "partition_head");
      HTML.Begin_Div (Class => "partition_name");
      HTML.Put_Paragraph (Label => "", Contents => Get_Name (P));
      HTML.Put_Paragraph (Label    => "Nodes",
                          Contents => Get_Nodes (P),
                          Class    => "nodelist");
      HTML.Put_Clearer;
      HTML.End_Div (Class => "partition_name");
      HTML.End_Div (Class => "partition_head");
      HTML.Begin_Div (Class => "partition_meta");
      Put_Meta;
      HTML.End_Div (Class => "partition_meta");
      HTML.Begin_Div (Class => "partition_access");
      Put_Access;
      HTML.End_Div (Class => "partition_access");
      HTML.Begin_Div (Class => "partition_resources");
      Put_Resources;
      HTML.End_Div (Class => "partition_resources");
      HTML.Put_Clearer;
      HTML.End_Div (Class => "partition_info");
   exception
      when E : others =>
      HTML.Comment (Exception_Message (E));
      HTML.End_Div (Class => "partition_info");
   end Put_Details;

   procedure Put_Error (Message : String) is
   begin
      HTML.Comment (Message);
   end Put_Error;

   procedure Put_List (List : Slurm.Partitions.List) is
   begin
      HTML.Begin_Div (Class => "partition_list");
      Ada.Text_IO.Put_Line ("<table><tr>");
      HTML.Put_Header_Cell (Data     => "Name");
      HTML.Put_Header_Cell (Data     => "Nodes");
      HTML.Put_Header_Cell (Data => "CPUs per GPU");
      Ada.Text_IO.Put ("</tr>");
      Iterate (List, Put'Access);
      Ada.Text_IO.Put_Line ("</table>");
      HTML.End_Div (Class => "partition_list");
   end Put_List;

end Partitions;
