-- @summary This package contains various routines that do not fit elsewhere
package Utils is
   Version : String := "v4.26"; -- Update Bugzilla when you change this

   User_Error : exception;
   -- an exception that indicates a user made an inconsistent request

   procedure Read_Link (Path : String; Buffer : out String; Last : out Natural);
   -- read the contents of a symbolic link; this is a front-end for
   -- the POSIX readlink function
   -- @param Path the path referring to the link
   -- @param Buffer the contents of the symbolic link; if the Buffer
   -- is larger than the contents of the link, the remainder of its
   -- contents are undefined; if the contents of the link do not fit
   -- the buffer, it will be filled with the first Buffer'Length
   -- characters of the contents
   -- @param Last the index of the last character used in the Buffer.
   -- Thus, Buffer (Buffer'First .. Last) is the contents of the
   -- symbolic link.
   -- @exception POSIX_ERROR if the POSIX function returned an error
   function To_String (Source : Integer) return String;
   -- Convert a number to a string. This function gets rid of the
   -- leading Space of the 'Img attribute, and it handles infinite
   -- quantities.
   -- @param Source the number to convert
   -- @return the decimal representation of Source, or the character
   -- entity for infinity if Source is infinite according to the
   -- definition of slurmlib, i.e. Integer'Last
   function Insert_Break (Source : String; C : Character) return String;
   -- insert zero-length whitespace into a given string after a given
   -- character, allowing the user agent to break it into lines
   -- @param Source the string to modify
   -- @param C the character after which to insert the break
   -- (typically a comma, period or the like)
   -- @return a copy of the Source, with wbr tags inserted

end Utils;
