--  @summary
--  Manipulate Slurm
--
--  @description
--  This package provides routines that change the status of
--  the queue as well as some helper routines.

package Actions is
   procedure Invoke (What : String)
     with Pre => (What /= "");
   -- Start an action based on a CGI parameter
   -- @param What the action to start

   procedure Assert_No_Root;
   -- For security reasons, make sure we are not running as root
   -- @exception Program_Error raised if we are running as root
   --  after all

   procedure Drop_Privileges;
   -- Drop root privileges and run as the user authenticated by
   -- the web server

   Permission_Error : exception;
end Actions;
