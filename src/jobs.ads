with Slurm.Jobs; use Slurm.Jobs;
with Slurm.Bunches;

-- @summary This package outputs jobs
package Jobs is

   Max_Name_Length : constant Positive := 20;
   -- job names up to this many characters will be shown in full

   function Name_As_HTML (J : Job) return String;
   -- format a job name as an HTML string, taking its length into
   -- account -- long names will be abbreviated, with the full name
   -- given as title (typically shown by user agents as mouseover
   -- text)
   -- @param J the job whose name should be returned
   -- @return a string containing the HTML-formatted name

   procedure Put_Details (ID : Natural);
   -- output a job with all its attributes
   -- @param ID the ID of the job to be shown
   procedure Put_List (Sort_By, Direction : String);
   -- output a list of all jobs currently loaded by libslurm;
   -- optionally sort the list by any column
   -- @param Sort_By the name of the column by which to sort; if it is
   -- the empty string, the list is unsorted
   -- @param Direction can be "inc", "dec", or empty; if it is "dec",
   -- sort in decreasing order; otherwise, sort is increasing
   procedure Put_Pending_List (Requirements : Slurm.Bunches.Set_Of_Requirements;
                               Sort_By, Direction : String);
   -- output a list of pending jobs that conform to a given set of
   -- requirements
   -- @param Requirements only output jobs that request these
   -- resources
   -- @param Sort_By the name of the column by which to sort; if it is
   -- the empty string, the list is unsorted
   -- @param Direction can be "inc", "dec", or empty; if it is "dec",
   -- sort in decreasing order; otherwise, sort is increasing
   procedure Put_Global_List (Sort_By, Direction : String);
   -- output a list of all jobs, loading them into libslurm first
   -- optionally sort the list by any column
   -- @param Sort_By the name of the column by which to sort; if it is
   -- the empty string, the list is unsorted
   -- @param Direction can be "inc", "dec", or empty; if it is "dec",
   -- sort in decreasing order; otherwise, sort is increasing
   procedure Put_Pending_List (Sort_By, Direction : String);
   -- output a list of all pending jobs, loading them into libslurm first
   -- optionally sort the list by any column
   -- @param Sort_By the name of the column by which to sort; if it is
   -- the empty string, the list is unsorted
   -- @param Direction can be "inc", "dec", or empty; if it is "dec",
   -- sort in decreasing order; otherwise, sort is increasing
   procedure Put_Reserving_List (Sort_By, Direction : String);
   -- output a list of all jobs with a backfill reservation,
   -- loading them into libslurm first;
   -- optionally sort the list by any column.
   --
   -- Note a backfill reservation is different from a reservation
   -- explicitly requested when calling sbatch. Jobs that have a
   -- backfill reservation are those that have a start time (in the
   -- future).
   -- @param Sort_By the name of the column by which to sort; if it is
   -- the empty string, the list is unsorted
   -- @param Direction can be "inc", "dec", or empty; if it is "dec",
   -- sort in decreasing order; otherwise, sort is increasing
   procedure Put_Running_List (Sort_By, Direction : String);
   -- output a list of all running jobs, loading them into libslurm first
   -- optionally sort the list by any column
   -- @param Sort_By the name of the column by which to sort; if it is
   -- the empty string, the list is unsorted
   -- @param Direction can be "inc", "dec", or empty; if it is "dec",
   -- sort in decreasing order; otherwise, sort is increasing
   procedure Put_User_List (User : String; Sort_By, Direction : String);
   -- output a list of all jobs by a given user, loading them into libslurm first
   -- optionally sort the list by any column
   -- @param User the user whose jobs are to be shown
   -- @param Sort_By the name of the column by which to sort; if it is
   -- the empty string, the list is unsorted
   -- @param Direction can be "inc", "dec", or empty; if it is "dec",
   -- sort in decreasing order; otherwise, sort is increasing
   function Explain_State (S : Extended_State) return String;
   -- give a user-facing explanation for a job state
   -- @param S the state that should be explained
   -- @return a string containing the explanation for S

   procedure Put_Summary;
   -- Output a summary, indicating how many jobs (and CPU requests)
   -- are in any of various common states. The last time the backfill
   -- scheduler ran is also shown.

end Jobs;
