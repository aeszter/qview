with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Slurm.Nodegroups;

-- @summary this package provides routines to write out node groups,
-- possibly with aggregate resources
package Nodegroups is

   procedure Put (G : Slurm.Nodegroups.Nodegroup);
   -- Output a nodegroup as a table row; depending on the status of
   -- the nodes in the group (e.g. nodes available, slots available,
   -- all nodes offline, nodes with an error), different html classes
   -- will be used so the row can be color-coded or otherwise marked.
   -- Meta groups (see slurmlib) will be written as collapsable
   -- sequences of rows.
   -- Data printed (as cells) include resources (GRES, features, RAM),
   -- number of nodes and CPUs, and an overview of their status.
   -- Any errors that have occurred (see slurm.loggers) will be
   -- printed as HTML comments, and the user will be alerted by a
   -- special html class given to that row.
   -- @param G the nodegroup to output
   procedure Put_List (Source : Slurm.Nodegroups.Summarized_List);
   -- Output a list of nodegroups as a table.
   -- @param Source the list of nodegroups to output
   procedure Put_Summary;
   -- Output aggregate resource usage
   procedure Put_All;
   -- output all nodegroups as a table
end Nodegroups;
