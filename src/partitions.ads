with Slurm.Partitions; use Slurm.Partitions;

-- @summary this package collects routines that show Slurm partitions
package Partitions is

   procedure Put_All;
   -- Outputs all partitions as a list
   procedure Put_Details (Name : String);
   -- Outputs one partition with all parameters
   -- @param Name the name of the partition to show
   procedure Put_List (List : Slurm.Partitions.List);
   -- Outputs a given list of partitions
   -- @param List the list of partitions to show
   function Explain_Mode (M : Slurm.Partitions.Preempt_Mode) return String;
   -- Give an explanation for a given Preempt_Mode that is suitable
   -- to be given to the user
   -- @param M the Preempt_Mode to explain
   -- @return a string that explains the meaning of M
end Partitions;
