with HTML;
with Ada.Text_IO;
with Ada.Strings.Fixed;

with Slurm.Loggers;
with Slurm.Nodes;
use Slurm.Nodes;
with Slurm.Hostlists; use Slurm.Hostlists;
with Slurm.Utils; use Slurm.Utils;
with Nodes;

package body Maintenance is

   procedure Put_Local_Menu;

   function High_Load (N : Nodes.Node) return Boolean is
   begin
      return Slurm.Nodes.Get_Load (N) > 1.1 * Slurm.Nodes.Get_Used_CPUs (N)
        + 0.1 * Slurm.Nodes.Get_Free_CPUs (N)
        and then not Slurm.Nodes.Is_Not_Responding (N);
   exception
      when Constraint_Error =>
         HTML.Error (To_String (Slurm.Nodes.Get_Name (N)) & " has inconsistent load");
         return False;
   end High_Load;

   function Low_Load (N : Nodes.Node) return Boolean is
   begin
      return Slurm.Nodes.Get_Load (N) < 0.9  * Slurm.Nodes.Get_Used_CPUs (N);
   end Low_Load;

   function Offline (N : Nodes.Node) return Boolean is
   begin
      return (Is_Not_Responding (N) and then Is_Draining (N)) or else
       Get_State (N) = NODE_STATE_DOWN;
   end Offline;

   procedure Put_All is
   begin
      Put_Local_Menu;
      Put_Error_Messages;
      Put_Drained;
      Put_Unreachable;
      Put_Offline;
      Put_High_Load;
      Put_Low_Load;
   end Put_All;

   procedure Put_Drained is
   begin
      HTML.Begin_Div (Class => "maintenance");
      HTML.Put_Anchor ("drained");
      HTML.Put_Heading  (Title => "Drained nodes",
                         Level => 3);
      HTML.Put_Heading (Title => "Node is drained/draining but not unreachable",
                        Level => 4);
      Nodes.Put_Selected (Reachable_Disabled'Access);
      HTML.End_Div (Class => "maintenance");
   end Put_Drained;

   procedure Put_Error_Messages is
      procedure Put_Error (Message : String);

      procedure Put_Error (Message : String) is
         use Ada.Strings.Fixed;

         Job_Start  : constant Natural := Index (Source  => Message,
                                                 Pattern => "of job") + 7;
         Job_End    : constant Natural := Index (Source => Message, Pattern => "'s",
                                                 From   => Job_Start);
         Host_Start : constant Natural := Index (Source  => Message,
                                                 Pattern => "at host") + 8;
         Host_End   : constant Natural := Message'Last + 1;
      begin
         if Job_Start > 0 and then Host_Start > 0 then
            Ada.Text_IO.Put_Line ("<li>" & Message (Message'First .. Job_Start - 1));
            HTML.Put_Link (Text       => Message (Job_Start .. Job_End - 1),
                           Link_Param => "job_id");
            Ada.Text_IO.Put_Line (Message (Job_End .. Host_Start - 1));
            HTML.Put_Link (Text => Message (Host_Start .. Host_End - 1),
                           Link_Param => "host");
            Ada.Text_IO.Put_Line (Message (Host_End .. Message'Last) & "</li>");
         else
            Ada.Text_IO.Put_Line ("<li>" & Message & "</li>");
         end if;
      end Put_Error;

   begin
      if not Slurm.Loggers.Errors_Exist then
         return;
      end if;

      HTML.Begin_Div (Class => "maintenance");
      HTML.Put_Heading  (Title => "Messages",
                         Level => 3);
      Ada.Text_IO.Put_Line ("<ul>");
      Slurm.Loggers.Iterate_Errors (Put_Error'Access);
      Ada.Text_IO.Put_Line ("</ul>");
      HTML.End_Div (Class => "maintenance");
   end Put_Error_Messages;

   procedure Put_Header is
   begin
      Ada.Text_IO.Put ("<tr>");
      HTML.Put_Header_Cell ("Node");
      HTML.Put_Header_Cell ("Slots");
      HTML.Put_Header_Cell ("Occupied");
      HTML.Put_Header_Cell ("Load");
      HTML.Put_Header_Cell ("per Core");
      HTML.Put_Header_Cell ("Swap");
      Ada.Text_IO.Put ("</tr>");
   end Put_Header;

   procedure Put_High_Load is
   begin
      HTML.Begin_Div (Class => "maintenance");
      HTML.Put_Anchor ("high_load");
      HTML.Put_Heading  (Title => "Overloaded Nodes",
                         Level => 3);
      HTML.Put_Heading (Title => "Load > 1.1 * Used_CPUs + 0.1 * Free_CPUs"
                        & " and Node is not unreachable",
                        Level => 4);
      Nodes.Put_Selected (High_Load'Access);
      HTML.End_Div (Class => "maintenance");
   end Put_High_Load;

   procedure Put_Local_Menu is
      procedure Put_Link (Data, Anchor : String);

      procedure Put_Link (Data, Anchor : String) is
      begin
         Ada.Text_IO.Put_Line
        ("<li><a href=""#" &
         Anchor &
         """>" &
         Data &
           "</a></li>");
      end Put_Link;
   begin
      HTML.Put_Navigation_Begin;
      Put_Link ("Drained", "drained");
      Put_Link ("Unreachable", "unreachable");
      Put_Link ("Offline", "offline");
      Put_Link (Data => "Overloaded", Anchor => "high_load");
      Put_Link ("Underutilized", "low_load");
      HTML.Put_Navigation_End;
   end Put_Local_Menu;

   procedure Put_Low_Load is
   begin
      HTML.Begin_Div (Class => "maintenance");
      HTML.Put_Anchor ("low_load");
      HTML.Put_Heading  (Title => "Underutilized Nodes",
                         Level => 3);
      HTML.Put_Heading (Title => "Load < 0.9 * Used_CPUs",
                        Level => 4);
      Nodes.Put_Selected (Low_Load'Access);
      HTML.End_Div (Class => "maintenance");
   end Put_Low_Load;

   procedure Put_Offline is
   begin
      HTML.Begin_Div (Class => "maintenance");
            HTML.Put_Anchor ("offline");
      HTML.Put_Heading  (Title => "Offline nodes",
                         Level => 3);
      HTML.Put_Heading (Title => "Node is either unreachable and drained, or down",
                        Level => 4);
      Nodes.Put_Selected (Offline'Access);
      HTML.End_Div (Class => "maintenance");
   end Put_Offline;

   procedure Put_Unreachable is
   begin
      HTML.Begin_Div (Class => "maintenance");
      HTML.Put_Anchor ("unreachable");
      HTML.Put_Heading  (Title => "Unreachable nodes",
                         Level => 3);
      HTML.Put_Heading (Title => "Node is unreachable but not drained or down",
                        Level => 4);
      Nodes.Put_Selected (Unreachable_Enabled'Access);
      HTML.End_Div (Class => "maintenance");
   end Put_Unreachable;

   function Reachable_Disabled (N : Nodes.Node) return Boolean is
   begin
      return Is_Draining (N) and then not Is_Not_Responding (N);
   end Reachable_Disabled;

   function Unreachable_Enabled (N : Nodes.Node) return Boolean is
   begin
      return Is_Not_Responding (N) and then not (Is_Draining (N) or else
      Get_State (N) = NODE_STATE_DOWN);
   end Unreachable_Enabled;

end Maintenance;
