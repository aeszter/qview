with Ada.Strings; use Ada.Strings;
with Ada.Strings.Fixed;
with Ada.Strings.Maps;
with POSIX.C; use POSIX.C;
with System;
with POSIX;
use POSIX;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package body Utils is

   function Insert_Break (Source : String; C : Character) return String is
      use Ada.Strings.Fixed;
      use Ada.Strings.Maps;

      Result : Unbounded_String;
      Position : Natural := Index (Source => Source,
                                   Set => To_Set (C),
                                   From    => Source'First);
      Prev : Natural := Source'First - 1;
   begin
      while Position /= 0 loop
         Result := Result & Source (Prev + 1 .. Position) & "<wbr>";
         Prev := Position;
         Position := Index (Source => Source,
                            Set    => To_Set (C),
                            From   => Prev + 1);
      end loop;
      return To_String (Result) & Source (Prev + 1 .. Source'Last);
   end Insert_Break;

   function readlink (path : char_ptr; buf : System.Address; bufsize : size_t)
     return ssize_t;
   pragma Import (C, readlink, "readlink");

   procedure Read_Link (Path : String; Buffer : out String; Last : out Natural) is
      Pathname_With_NUL : constant POSIX_String := To_POSIX_String (Path) & POSIX.NUL;
      Result : ssize_t;
   begin
      Result := readlink (Pathname_With_NUL (Pathname_With_NUL'First)'Unchecked_Access,
                          Buffer (Buffer'First)'Address,
                          size_t (Buffer'Last - Buffer'First + 1));
      if Result = -1 then
         raise POSIX.POSIX_Error;
      end if;
      Last := Buffer'First + Integer (Result) - 1;
   end Read_Link;

   function To_String (Source : Integer) return String is
   begin
      if Source = Integer'Last then
         return "&infin;";
      end if;
      return Ada.Strings.Fixed.Trim (Source'Img, Left);
   end To_String;

end Utils;
