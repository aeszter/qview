with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Containers.Doubly_Linked_Lists; use Ada.Containers;
with Calendar;
with CGI;
with Slurm.Utils; use Slurm.Utils;
with Slurm.Gres;
with Slurm.Hostlists;
with Slurm.Tres;

  -- @summary
  -- This package collects various routines that output HTML
  -- code. While it is possible (and sometimes makes sense) to write
  -- output directly with Ada.Text_IO, in most cases such calls should
  -- be transferred to the HTML package.
package HTML is
   procedure Begin_Form;
   -- Start an HTML form; call End_Form to finish it.
   procedure End_Form;
   -- End an HTML form
   procedure Put_Cell (Data       : String;
                       Link_Param : String := "";
                       Acronym    : String := "";
                       Tag        : String := "td";
                       Class      : String := "";
                       Colspan    : Positive := 1);
   --  write out a single table cell, optionally changing the tag (to th)
   --  optionally, link the contents as "Link_Param=Data"
   --  @param Data the text to put inside the cell
   --  @param Link_Param if set, add a link of the form
   --  Link_Param=Data
   --  @param Acronym if set, Data is treated as an
   --  acronym/abbreviation, and this parameter contains the long form
   --  @param Tag the tag used to define the cell; usually td (the
   --  default) or th (for a header)
   --  @param Class the class to attach to the cell (for CSS)
   --  @param Colspan how many columns the cell should cover
   procedure Put_Cell (Data       : Unbounded_String;
                       Link_Param : String := "";
                       Tag        : String := "td";
                       Class      : String := "");
   --  write out a single table cell, optionally changing the tag (to th)
   --  optionally, link the contents as "Link_Param=Data"
   --  @param Data the text to put inside the cell
   --  @param Link_Param if set, add a link of the form
   --  Link_Param=Data
   --  @param Tag the tag used to define the cell; usually td (the
   --  default) or th (for a header)
   --  @param Class the class to attach to the cell (for CSS)

   procedure Put_Img_Cell (Image : String; Extra_Text : String := "");
   -- write out a table cell containing an image
   -- @param Image the name of the image, without path or the .png
   -- extension. It will also be used as alt and title text
   -- @param Extra_Text will displayed after the image
   procedure Put_Img (Name, Text, Link : String; Extra_Args : String := "");
   -- write out a linked image
   -- @param Name the name of the image, without path or the .png
   -- extension
   -- @param Text the alt and title text
   -- @param Link the URL the image will be linked to
   -- @param Extra_Args any further arguments to the a tag
   procedure Put_Img_Form (Name, Text, Action : String);
   -- write an image as an active form element (use after Start_Form)
   -- @param Name the name of the image, without path or .png
   -- extension
   -- @param Text the alt and title text
   -- @param Action the name of the input element; this used to be
   -- sent as act=Action from Safari, but (at least) Firefox doesn't;
   -- see Bug #2296

   function Img_Tag (Image : String) return String;
   -- format an img tag
   -- @param Image the name of the image, without path or .png
   -- extension; also used as alt and title text
   -- @return the complete tag containing the full image path as well
   -- as alt and title text

   procedure Put_Hidden_Form (Name, Value : String);
   -- write out a hidden form element (after Start_Form). This can be
   -- used to transfer some data that's not supposed to be
   -- user-editable along with other data gathered through a form.
   -- Caution: never trust what you receive here, this is still
   -- user-controlled!
   -- @param Name the element's name
   -- @param Value the element's value

   procedure Put_Time_Cell (Time : Calendar.Time);
   -- write out a table cell with a point in time. Values that are close
   -- to now will have their days given as yesterday, today or
   -- tomorrow instead of an explicit date.
   -- @param Time the value to write out
   procedure Put_Duration_Cell (Secs : Natural);
   -- write out a table cell with a duration, formatted in a
   -- reasonable way (hours, minutes, seconds; or days, hours,
   -- minutes, seconds).
   -- @param Secs the number of seconds to display
   procedure Put_Duration_Cell (Span : Duration);
   -- write out a table cell with a duration, formatted in a
   -- reasonable way. If Span is negative, write "expired" instead.
   -- @param Span the duration to display
   procedure Put_Header_Cell (Data     : String;
                              Acronym  : String  := "";
                              Sortable : Boolean := True;
                              Colspan  : Positive := 1);
   -- write out a header cell, optionally with support for sorting the
   -- table
   -- @param Data the text inside the cell
   -- @param Acronym if set, Data is taken to be an abbreviation
   -- with Acronym containing the full text
   -- @param Sortable if True, clicking on the cell will instruct the
   -- application to sort by this column (using the sort CGI
   -- parameter); clicking it again will reverse sort order using a
   -- cookie;
   -- if Sortable is false, the subprogram acts like Put_Cell with the
   -- tag set to "th"
   -- @param Colspan how many columns the cell should cover
   procedure Put_Edit_Box (Name, Default : String);
   -- write out a text entry box (after Start_Form)
   -- @param Name the name of the box (used when submitting the form)
   -- @param Default the default value shown in the box when the page is
   -- first displayed
   procedure Put_Search_Box;
   --  put a text input element used to search for a job or user; it
   --  brings its own little HTML form, so do not call Start_Form
   --  first. Name and default value are "search"

   procedure Put_Navigation_Begin;
   -- start an unordered list meant to serve as site navigation; add
   -- items with Put_Navigation_Link and end with Put_Navigation_End
   procedure Put_Navigation_End;
   -- end an unordered list (site navigation) started with
   -- Put_Navigation_Begin
   procedure Put_Navigation_Link (Data : String; Link_Param : String);
   -- add a site navigation entry; call Put_Navigation_Begin first.
   -- @param Data the title of the entry
   -- @param Link_Param the CGI parameter of the entry; it should be
   -- given as "name=value"

   procedure Put_Paragraph (Label : String; Contents : String; Class : String := "");
   -- write out a paragraph of the form "Label: Contents"; if the
   -- Label is the empty string, the paragraph will be
   -- "Contents" instead
   -- @param Label describes what is shown (e.g. Node State)
   -- @param Contents the value (e.g. Down)
   -- @param Class a class for CSS
   procedure Put_Paragraph (Label : String; Contents : Calendar.Time; Class : String := "");
   -- write out a paragraph of the form "Label: Contents"; if the
   -- Label is the empty string, the paragraph will be
   -- "Contents" instead
   -- @param Label describes what is shown (e.g. Boot Time)
   -- @param Contents the value
   -- @param Class a class for CSS
   procedure Put_Paragraph (Label : String; Contents : Duration; Class : String := "");
   -- write out a paragraph of the form "Label: Contents"; if the
   -- Label is the empty string, the paragraph will be
   -- "Contents" instead
   -- @param Label describes what is shown (e.g. Job Runtime)
   -- @param Contents the value
   -- @param Class a class for CSS
   procedure Put_Paragraph (Label : String; Contents : Boolean; Class : String := "");
   -- write out a paragraph of the form "Label: Contents"; if the
   -- Label is the empty string, the paragraph will be
   -- "Contents" instead. In each case, the Boolean value is
   -- represented by an icon.
   -- @param Label describes what is shown (e.g. Node Shared)
   -- @param Contents the value
   -- @param Class a class for CSS
   procedure Put_Paragraph (Label : String; Contents : Unbounded_String; Class : String := "");
   -- write out a paragraph of the form "Label: Contents"; if the
   -- Label is the empty string, the paragraph will be
   -- "Contents" instead
   -- @param Label describes what is shown (e.g. Node State)
   -- @param Contents the value (e.g. Down)
   -- @param Class a class for CSS
   procedure Put_Paragraph (Label : String; Contents : Slurm.Hostlists.Node_Name;
                            Class : String := "");
   -- write out a paragraph of the form "Label: Contents"; if the
   -- Label is the empty string, the paragraph will be
   -- "Contents" instead
   -- @param Label describes what is shown (e.g. Name)
   -- @param Contents the value
   -- @param Class a class for CSS
   procedure Put_Paragraph (Label    : Unbounded_String;
                            Contents : Unbounded_String;
                            Class    : String := "");
   -- write out a paragraph of the form "Label: Contents"; if the
   -- Label is the empty string, the paragraph will be
   -- "Contents" instead
   -- @param Label describes what is shown (e.g. Node State)
   -- @param Contents the value (e.g. Down)
   -- @param Class a class for CSS
   procedure Put_Link (Label : String; ID : String; Link_Param : String);
   -- write out a paragraph of the form "Label: ID" where ID
   -- links to the CGI with the parameter Link_param=ID
   -- @param Label the label of the paragraph, e.g. "Job ID"
   -- @param ID the linked text, e.g. "123"
   -- @param Link_Param the name of the parameter in the link, e.g.
   -- "job_id"
   procedure Put_Link (Text, Link_Param : String);
   -- write out a text, and have it link to Link_Param=Text
   -- @param Text the text to be written
   -- @param Link_Param the name of the parameter in the link
   procedure Comment (Data : String);
   -- write an HTML comment
   -- @param Data the text inside the comment
   procedure Comment (Data : Unbounded_String);
   -- write an HTML comment
   -- @param Data the text inside the comment
   procedure Bug_Ref (Bug_ID : Positive; Info : String);
   -- write a comment containing a link to a bug in Bugzilla and the
   -- string given as Info. The Bugzilla URL is taken from the
   -- environment variable BUGZILLA_URL
   -- @param Bug_ID the ID of the bug to link to
   -- @param Info the extra string to put inside the comment
   function Bug_Ref (Bug_ID : String) return String;
   -- format a link to a bug in Bugzilla; the URL ist taken from the
   -- environment variable BUGZILLA_URL
   -- @param Bug_ID the ID of the bug to link to
   -- @return the formatted link
   procedure Put (Data : Boolean);
   -- write out Boolean data as an icon (tick for true, cross for
   -- false)
   -- @param Data the Boolean value to be written
   procedure Put_List (List : String_Sets.Set);
   -- write a set of strings as an unordered list (ul); if the set
   -- is empty, a cross icon is shown
   -- @param List the set of strings to write
   procedure Put_List (List : Slurm.Hostlists.Hostlist);
   -- write a list of host names as an unordered list (ul); if the
   -- list is empty, a cross icon is shown
   -- @param List the set of hostnames to write
   procedure Put_List_Head;
   -- start an unordered list
   procedure Put_List_Tail;
   -- end an unordered list started with
   -- Put_List_Head
   procedure Put_List_Entry (Key, Element : String);
   -- write an entry into a list started with Put_List_Head;
   -- the entry will be of the form "Key: Element"
   -- @param Key the name of the entry
   -- @param Element the value of the entry
   procedure Put_List_Entry (Element : String);
   -- write a simple entry into a list started with Put_List_Head;
   -- @param Element the value of the entry
   procedure Put_Empty_List;
   -- write an icon representing an empty list

   procedure Put_Opensearch (URL : String);
   --  write a reference to an open search description so it can be
   --  used by the user agent
   -- @param URL the URL of the open search description
   procedure Put_Stylesheet (URL : String);
   --  write a reference to a CSS style sheet so it can be
   --  used by the user agent
   -- @param URL the URL of the style sheet
   procedure Put_Clearer;
   -- write an empty div of class "clearer"; this can be used to
   -- expand certain flexible boxes to cover all contained elements
   procedure Error (Message : String);
   -- report a program error to the user. This included the error
   -- message along with a link to a pre-filled Bugzilla bug entry
   -- form. The version of the application and of slurmlib are passed
   -- to Bugzilla, so they're always present in the bug.
   -- @param Message the error message; it is displayed by the user
   -- agent and also passed on to Bugzilla as the title of the bug
   procedure Put_Anchor (Tag : String);
   -- write an anchor tag for use as a link target
   -- @param Tag the name of the anchor
   procedure Put_Heading (Title : String; Level : Positive);
   -- write an HTML heading (h1 to h6)
   -- @param Title the title of the heading
   -- @param Level the level of the heading, e.g. 2 for h2

   function Param (Name : String; Index : in Positive := 1;
               Required : in Boolean := False) return String renames CGI.Value;
   -- retrieve the value of a CGI parameter, e.g. if the CGI has been
               -- called with ?a=1&b=2&b=0&c=3 =>
               -- Param ("c") yields "3" and Param("b",2) yields "0"
               -- @param Name the name of the parameter
               -- @param Index a parameter may be given multiple
               -- times; this function will return the Index'th value
               -- @param Required if True, raise Constraint_Error if
               -- the parameter cannot be found; otherwise, return the
               -- empty string
               -- @exception Constraint_Error raised if Required is
               -- True but the parameter doesn't exist
               -- @return the value of the parameter
   function Param_Is (Param : String; Expected : String) return Boolean;
   -- check a CGI parameter for a given value
   -- @param Param the name of the parameter
   -- @param Expected the expected value of the parameter
   -- @return True iff the value of the parameter Param equals
   -- Expected
   function Strip_Parameter (Source, Key : String) return String;
   -- given a string containing CGI parameters, remove the parameter
   -- named Key
   -- @param Source a string consisting of CGI parameters
   -- @param Key the name of a parameter to remove
   -- @return a string containing all Parameters (and values) of
   -- Source except for Key (and its value)
   procedure Begin_Div (Class : String := ""; ID : String := "");
   -- start a div, optionally with the given class and ID. This
   -- subprogram will record the open div to aid with proper nesting,
   -- and to ensure all open divs are later closed.
   -- @param Class the class of the div
   -- @param ID the ID of the div
   procedure End_Div (Class : String := ""; ID : String := "");
   -- end a div, optionally checking proper nesting. If a class or
   -- ID are given, an error will be reported to the user (via the
   -- Error subprogram) if the div opened last doesn't have the same
   -- class or ID.
   -- If End_Div is called but no open div is registered, an error
   -- is reported as well.
   -- @param Class the class of the div; if empty, it will not be
   -- checked
   -- @param ID the ID of the div; if empty, it will not be checked
   procedure Finalize_Divs (Silent : Boolean := False);
   -- end all open divs, optionally reporting an error to the user.
   -- @param Silent if False, report an error if any divs are still
   -- open when Finalize_Divs is called
   function Encode (S : String) return String renames CGI.HTML_Encode;
   -- Encode special characters as HTML charachter entities
   -- @param S the string to be encoded
   -- @return a copy of S, with all special characters replaced by
   -- HTML charachter entities (e.g. > becomes &gt;
   function Acronym (Short, Long : String) return String;
   --  Purpose: Compose an acronym tag
   --  @param Short the abbreviation to be displayed inline
   --  @param Long the explanation/long name typically shown as
   --  mouseover by user agents
   --  @return the Short string formatted as an acronym, with the
   --  Long string as explanation
   --  for use in Ada.Text_IO.Put or other HTML subprograms

   function Help_Icon (Topic : String) return String;
   --  Purpose: generate a string that contains html tags to display a help
   --  icon and link to an external help page (in the wiki)
   --  @param Topic Help topic to be included in the link
   --  @Return generated string, for inclusion in Put_Cell, Put_Paragraph
   --  or the like

   function To_String (Time : Calendar.Time) return String;
   -- convert a point in time to a string; if possible, the date is
   -- displayed as yesterday, today or tomorrow rather than
   -- numerically
   -- @param Time the point in time to display
   -- @return the representation of Time
   function To_String (Span : Duration) return String;
   -- convert a duration to a string; special values infinite and
   -- no_val are respected
   -- @param Span the duration to display
   -- @return the representation of Span
   function To_String (List : Slurm.Gres.List; Max_Items : Positive := 99) return String;
   -- convert a list of Gres to a comma-separated string
   -- @param List the list to convert
   -- @param Max_Items the maximum number of items to display; any
   -- extra items will be represented as "..."
   -- @return a string containing the comma-separated items of List
   function To_String (List : Slurm.Tres.List; Max_Items : Positive := 99) return String;
   -- convert a list of TRES to a comma-separated string
   -- @param List the list to convert
   -- @param Max_Items the maximum number of items to display; any
   -- extra items will be represented as "..."
   -- @return a string containing the comma-separated items of List
   function To_Web_String (List : Slurm.Gres.List; Max_Items : Positive := 99) return String;
   -- convert a list of Gres to a comma-separated string for use in
   -- URLs
   -- @param List the list to convert
   -- @param Max_Items the maximum number of items to display; any
   -- extra items will be represented as "..."
   -- @return a string containing the comma-separated items of List
   function To_Web_String (List : Slurm.Tres.List; Max_Items : Positive := 99) return String;
   -- convert a list of Gres to a comma-separated string for use in
   -- URLs
   -- @param List the list to convert
   -- @param Max_Items the maximum number of items to display; any
   -- extra items will be represented as "..."
   -- @return a string containing the comma-separated items of List
   function Get_Action_URL (Action, Params : String) return String;
   -- generate a URL to perform an action. For security reasons, this
   -- will point to a different CGI binary.
   -- @param Action the action to perform
   -- @param Params any further CGI parameters
   -- @return the URL generated
   function Current_URL return String;
   -- @return the URL that has led to the invocation of the current
   -- program run, including CGI parameters
private
   type Div is record
      Class : Unbounded_String;
      ID    : Unbounded_String;
   end record;

   package Div_Lists is new Doubly_Linked_Lists (Element_Type => Div);

   Div_List : Div_Lists.List;
   Form_Open : Boolean := False;
end HTML;
