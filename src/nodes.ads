with Slurm.Nodes; use Slurm.Nodes;
with Slurm.Node_Properties;
with Slurm.Partitions;

-- @summary This package provides subprograms to show nodes to the
-- user.
package Nodes is
   subtype Node is Slurm.Nodes.Node;

   procedure Put_All;
   -- Show all nodes as a list
   procedure Put_Details (Name : String);
   -- Show one node with all its information
   -- @param Name the name of the nonde to show
   procedure Put_List (List : Slurm.Nodes.List);
   -- Show a list of nodes
   -- @param List the list of nodes to show
   procedure Put_For_Maintenance (List : Slurm.Nodes.List);
   -- show a list of nodes in a way that's optimal for the maintenance
   -- page, not for normal, user-oriented pages
   -- @param List the list of nodes to show
   procedure Put_List (Properties : Slurm.Node_Properties.Set_Of_Properties);
   -- show a list of all nodes that have certain properties
   -- @param Properties the properties a node must have to be included
   -- in the list
   procedure Put_Equivalent (Name : String; State : String := "");
   -- show a list of nodes that have the same properties as a given
   -- node
   -- @param Name the name of the model node that shall be used to
   -- determine the properties that will, in turn, be used to select
   -- the nodes to list
   -- @param State if set, the list of nodes will be limited to those
   -- in the given state
   function Explain_State (S : states) return String;
   -- explain the meaninng of a node state to the user
   -- @param S the state to explain
   -- @return an explanation for S that can be displayed to the user

   procedure Init (Properties : out Slurm.Node_Properties.Set_Of_Properties;
                   GRES, TRES, Memory, CPUs, Features : String);
   -- combine given node properties into a record
   -- @param Properties upon return, a record initialized with the
   -- properties given
   -- @param GRES generic resources (typically GPUs)
   -- @param TRES trackable resources, typically CPUs and RAM
   -- @param Memory the amount of RAM
   -- @param CPUs the cpus of the node
   -- @param Features the features of the nnode
   procedure Put_Selected (Selector : not null access function (N : Node) return Boolean);
   -- show a list of nodes that are selected by an arbitrary function
   -- @param Selector a boolean function that is called once for every
   -- node; the list will include exactly those nodes for which
   -- Selector returns True
   -- @param N will contain the node
   procedure Put_Summary (List : Slurm.Nodes.List);
   -- Output a summary, indicating how many nodes there
   -- are in any of the basic states.

private

   procedure Put_Partition (P : Slurm.Partitions.Partition);

--     procedure Put (H : SGE.Hosts.Host);
   procedure Put_Jobs (ID : Positive; N : Node);
--     procedure Put_For_Maintenance (H : SGE.Hosts.Host);
end Nodes;
